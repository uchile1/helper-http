package helperHttp

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/uchile1/helper/helperCommon"
	"gitlab.com/uchile1/helper/helperLog"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmhttp"
	"moul.io/http2curl"
)

// New crea una nueva instancia de HttpUtil con valores por defecto
func New(ctx context.Context) HttpUtil {
	logger := log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	logger = logger.Level(helperLog.Logger.GetLevel())
	client := apmhttp.WrapClient(&http.Client{})
	defaultHeaders := http.Header{
		"Content-Type": []string{"application/json"},
	}
	return HttpUtil{
		logger:    logger,
		client:    client,
		header:    &defaultHeaders,
		callRetry: 1,
		ctx:       ctx,
	}
}

// HttpUtil estructura principal para realizar solicitudes HTTP
type HttpUtil struct {
	logger         zerolog.Logger
	client         *http.Client
	header         *http.Header
	callRetry      int
	ctx            context.Context
	printCurl      bool
	authentication *authenticacionRest
}

type authenticacionRest struct {
	User string
	Pass string
}

type ResponseData struct {
	Data       []byte
	StatusCode int
}

// SetLevelLog establece el nivel de log
func (l *HttpUtil) SetLevelLog(logLevel string) {
	if logLevel != "" {
		lev, err := zerolog.ParseLevel(logLevel)
		if err == nil {
			l.logger = l.logger.Level(lev)
		}
	}
}

// SetCallRetry establece el número de reintentos para las solicitudes
func (l *HttpUtil) SetCallRetry(callRetry int) {
	l.callRetry = callRetry
}

// SetHeader establece las cabeceras de la solicitud
func (l *HttpUtil) SetHeader(header http.Header) {
	l.header = &header
}

// AddHeader añade cabeceras a la solicitud
func (l *HttpUtil) AddHeader(header http.Header) {
	if l.header == nil {
		l.header = &http.Header{}
	}
	for k, v := range header {
		for _, val := range v {
			l.header.Add(k, val)
		}
	}
}

// SetClient establece el cliente HTTP
func (l *HttpUtil) SetClient(client http.Client) {
	l.client = &client
}

// SetPrintCurl habilita o deshabilita la impresión de las solicitudes en formato curl
func (l *HttpUtil) SetPrintCurl(arg bool) {
	l.printCurl = arg
}

// SetBasicAuthenticacion establece la autenticación básica
func (l *HttpUtil) SetBasicAuthenticacion(user, pass string) {
	l.authentication = &authenticacionRest{User: user, Pass: pass}
}

// QueryGraphql realiza una consulta GraphQL
func (l *HttpUtil) QueryGraphql(url string, query string, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("---------------QueryGraphql----------------------------")
	triggerSpan(l)
	jsonData := map[string]string{"query": query}
	return l.rest(url, http.MethodPost, jsonData, timeout)
}

// MutationGraphql realiza una mutación GraphQL
func (l *HttpUtil) MutationGraphql(url string, mutation string, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("-----------------MutationGraphql--------------------------")
	triggerSpan(l)
	jsonData := map[string]string{"query": mutation}
	return l.rest(url, http.MethodPost, jsonData, timeout)
}

// GetRest realiza una solicitud GET
func (l *HttpUtil) GetRest(url string, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("-----------------GetRest--------------------------")
	triggerSpan(l)
	return l.rest(url, http.MethodGet, nil, timeout)
}

// PostRest realiza una solicitud POST
func (l *HttpUtil) PostRest(url string, body interface{}, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("-----------------PostRest--------------------------")
	triggerSpan(l)
	return l.rest(url, http.MethodPost, body, timeout)
}

// PutRest realiza una solicitud PUT
func (l *HttpUtil) PutRest(url string, body interface{}, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("-----------------PutRest--------------------------")
	triggerSpan(l)
	return l.rest(url, http.MethodPut, body, timeout)
}

// PatchRest realiza una solicitud PATCH
func (l *HttpUtil) PatchRest(url string, body interface{}, timeout time.Duration) (ResponseData, error) {
	l.logger.Debug().Msg("-----------------PatchRest--------------------------")
	triggerSpan(l)
	return l.rest(url, http.MethodPatch, body, timeout)
}

func (l *HttpUtil) rest(url string, method string, body interface{}, timeout time.Duration) (ResponseData, error) {
	t := time.Now()
	var span *apm.Span
	ctx := l.ctx
	if ctx != nil {
		span, ctx = apm.StartSpan(ctx, helperCommon.GetFrame(1).Function, "func")
		defer span.End()
	}

	var response *http.Response
	var err error

	defer func() {
		if response != nil && response.Body != nil {
			response.Body.Close()
		}
		if err != nil && ctx != nil {
			apm.CaptureError(ctx, err).Send()
		}
		l.logger.Debug().Msgf("---REST--- Demora en milisegundos: %d", time.Since(t).Milliseconds())
	}()

	bodyR, err := prepareRequestBody(body, l.logger)
	if err != nil {
		return ResponseData{nil, http.StatusInternalServerError}, err
	}

	request, err := http.NewRequest(method, url, bodyR)
	if err != nil {
		l.logger.Error().Msgf("<<<ERROR url: %s method: %s no se pudo crear 'NewRequest': %v\n", url, method, err)
		return ResponseData{nil, http.StatusInternalServerError}, err
	}

	request.Header = *l.header
	if l.authentication != nil {
		request.SetBasicAuth(l.authentication.User, l.authentication.Pass)
	}

	client := *l.client
	client.Timeout = timeout

	if l.printCurl {
		printCurlCommand(request, l.logger)
	}

	for i := 0; i < l.callRetry; i++ {
		response, err = executeRequest(client, request, ctx, l.logger, method, url, l.callRetry, i)
		if err == nil {
			break
		}
	}

	if err != nil {
		l.logger.Error().Msgf("<<<ERROR Request: %v\n", err)
		return ResponseData{nil, http.StatusInternalServerError}, err
	}

	data, err := readResponseBody(response, l.logger)
	if err != nil {
		return ResponseData{nil, response.StatusCode}, err
	}

	l.logger.Debug().Msgf("+++REST+++ respuesta servicio: %s", data)
	return ResponseData{data, response.StatusCode}, nil
}

func prepareRequestBody(body interface{}, logger zerolog.Logger) (io.Reader, error) {
	if body == nil {
		return nil, nil
	}
	if reader, ok := body.(io.Reader); ok {
		return reader, nil
	}
	aux, err := json.Marshal(body)
	if err != nil {
		logger.Error().Msgf("<<<ERROR al codificar json a bytes. ---> %s", err.Error())
		return nil, err
	}
	return bytes.NewReader(aux), nil
}

func printCurlCommand(request *http.Request, logger zerolog.Logger) {
	command, _ := http2curl.GetCurlCommand(request)
	logger.Info().Msgf("## CURL--> %s", command)
}

func executeRequest(client http.Client, request *http.Request, ctx context.Context, logger zerolog.Logger, method string, url string, callRetry int, attempt int) (*http.Response, error) {
	response, err := client.Do(request.WithContext(ctx))
	if err != nil && errors.Is(err, context.Canceled) && attempt < callRetry-1 {
		logger.Warn().Msgf("<<<ERROR [%v] al realizar %s a la url: %s con intentos restantes: %d", err, method, url, callRetry-attempt-1)
		time.Sleep(5 * time.Second)
	}
	return response, err
}

func readResponseBody(response *http.Response, logger zerolog.Logger) ([]byte, error) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(response.Body)
	if err != nil {
		logger.Error().Msgf("<<<ERROR Reading data: %v\n", err)
		return nil, err
	}
	return buf.Bytes(), nil
}

func triggerSpan(l *HttpUtil) {
	if l.ctx != nil {
		span, ctx := apm.StartSpan(l.ctx, helperCommon.GetFrame(2).Function, "func")
		l.ctx = ctx
		defer span.End()
	}
}
