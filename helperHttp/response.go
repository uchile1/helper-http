package helperHttp

import (
	"fmt"
)

var ERR_GENERIC = "ERR_INTERNAL"

// @name Response
type Response struct {
	Status    string      `json:"status"`
	Message   *string     `json:"message,omitempty"`
	ErrorCode *string     `json:"error_code,omitempty"`
	Data      interface{} `json:"data,omitempty"`
}

func NewSuccessResponse(data interface{}) Response {
	return Response{
		Status:    "success",
		Message:   nil,
		ErrorCode: nil,
		Data:      data,
	}
}

func NewErrorResponse(message string) Response {
	return Response{
		Status:    "error",
		Message:   &message,
		ErrorCode: nil,
	}
}

func NewErrorCodeResponse(message, errorCode string) Response {
	if errorCode == "" {
		errorCode = ERR_GENERIC
	}
	return Response{
		Status:    "error",
		Message:   &message,
		ErrorCode: &errorCode,
	}
}

func (e *Response) Error() *ErrorResponse {
	if e.Message == nil {
		return nil
	}
	return &ErrorResponse{
		Message:   *e.Message,
		ErrorCode: e.ErrorCode,
	}
}

type ErrorResponse struct {
	Message   string
	ErrorCode *string
}

func (e *ErrorResponse) ToString() string {
	if e.ErrorCode != nil {
		return fmt.Sprintf("%s (code: %s)", e.Message, *e.ErrorCode)
	}
	return e.Message
}
