package helperRabbitMQ

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/uchile1/helper/helperCommon"
)

// @name Amqp
type Amqp struct {
	Connection *amqp091.Connection
	Channel    *amqp091.Channel
	Queue      amqp091.Queue
}

func GetConnection() *Amqp {
	usu := os.Getenv("RABBITMQ_USER")
	pass := os.Getenv("RABBITMQ_PASS")
	url := os.Getenv("RABBITMQ_URL")
	conn, err := amqp091.Dial(fmt.Sprintf("amqp://%s:%s@%s", usu, pass, url))
	helperCommon.PanicOnError(err, "Failed to connect to RabbitMQ")
	return &Amqp{Connection: conn}
}

func (amqp *Amqp) GetChannel() {
	ch, err := amqp.Connection.Channel()
	helperCommon.PanicOnError(err, "Failed to open a channel")
	amqp.Channel = ch
}

func (amqp *Amqp) GetQueue(name string) {
	q, err := amqp.Channel.QueueDeclare(
		name,  // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	helperCommon.PanicOnError(err, "Failed to declare a queue")
	amqp.Queue = q
}

func (amqp *Amqp) ProduceMessage(body []byte) error {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	return amqp.Channel.PublishWithContext(ctx,
		"",              // exchange
		amqp.Queue.Name, // routing key
		false,           // mandatory
		false,           //immediate
		amqp091.Publishing{
			DeliveryMode: amqp091.Persistent,
			ContentType:  http.DetectContentType(body),
			Body:         body,
		})
}

func (amqp *Amqp) ConsumeConfigureQualityOfService() error {
	return amqp.Channel.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
}
