package main

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/uchile1/helper/helperHttp"
)

func TestGetConHeaders(t *testing.T) {
	origin := "https://appexternal-example-desa.uchile.cl"
	appKey := "varloq"
	header := map[string][]string{}
	header["AppKey"] = []string{appKey}
	header["Origin"] = []string{origin}
	util := helperHttp.New(nil)
	util.SetHeader(header)
	rut := "013716521K"
	resp, err := util.GetRest(fmt.Sprintf("https://api-desa.uchile.cl/v1/personas?indiv_id=\"%s\"", rut), 30*time.Second)
	if err != nil {
		t.Errorf("error: %v", err)
		return
	}
	t.Logf("salida: %s con status code: %d", string(resp.Data), resp.StatusCode)
}
