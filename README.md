##  HELPER HTTP

Utilitario para realizar invocaciones REST seguiendo las buenas prácticas de los desarrollos Uchile, para bajar librería en tu proyecto:

    
   ` go get gitlab02.uchile.cl/desarrollo-vti/helper-http`    


**MODO DE USO:**

1. Primero se hace una instancia del util

    ```
    util := httpUtil.New(contexto)
    ```
    * El contexto solo se usa para registro del APM de elastic, si no te interesa el APM, solo debes pasar un *'nil'* como parámetro

2. Se realiza invocación REST (GET, POST, DELETE, PATCH):

    ```
    resp, err := util.GetRest("<<<URL_SERVICIO>>>", 30*time.Second)
    ```
    
    * Párametros de entrada:     

        *url* : la url del servicio externo que hace invocación

        *time*: cantidad de tiempo de espera de la incocación REST

    * Párametros de salida:  

        *respuesta* : struct del tipo responseData que tiene 2 atributos: *Data* que contiene un []byte con la respuesta del servicio y el *StatusCode* con el código de estado de la respuesta del servicio

        *error* : con el error de la invocación

3. Más usos

    * util.SetHeader(newHeader): Para agregar más headers en la invocación del servicio, se usa mucho para el uso de los servicios de plataforma habilitante

    * util.AddHeader(header http.Header): para ir agregando header a la lista de headers

    * util.SetCallRetry(callRetry int): Recibe un número entero para hacer reintentos de la invocación en caso de error

    * util.SetClient(client http.Client): En caso de no querer usar el cliente por defecto que gatilla el request, se puede setear uno personalizado

    * util.SetPrintCurl(arg bool): True, imprime CURL por consola como log de INFO. False (default): no imprime CURL

    * util.SetBasicAuthenticacion(user, pass string): Setear autenticación en la invocación request para recursos protegidos

    * util.SetLevelLog(logLevel string): Para hacer más verboso los logs en la invocación http, según [nivel de log de logrus](https://github.com/sirupsen/logrus#level-logging)

## BASE DE DATOS

## EMAIL

## RUT

## OTROS
