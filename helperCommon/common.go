package helperCommon

import (
	"bytes"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net"
	"reflect"
	"runtime"
	"strconv"

	"gitlab.com/uchile1/helper/helperLog"
)

/*
*
Retorna el frame en tiempo runtime
*/
func GetFrame(level int) runtime.Frame {
	pc := make([]uintptr, 15)
	n := runtime.Callers(level+1, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame
}

type anyArray = interface{}

func JoinpSlice(src anyArray, stringSeparator string) string {
	dst := ""
	switch reflect.TypeOf(src).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(src)
		for i := 0; i < s.Len(); i++ {
			value := s.Index(i)
			//fmt.Println("type: ", s.Index(i).Type())
			switch s.Index(i).Type().Kind() {
			case reflect.String:
				dst += value.String() + stringSeparator
			case reflect.Int:
				dst += strconv.FormatInt(value.Int(), 10) + stringSeparator
			case reflect.Bool:
				dst += strconv.FormatBool(value.Bool()) + stringSeparator
			case reflect.Float32, reflect.Float64:
				dst += strconv.FormatFloat(value.Float(), 'f', 6, 64) + stringSeparator
			default:
				dst += value.String() + stringSeparator
			}
			//fmt.Println(dst)
		}
	}
	return dst[:len(dst)-len(stringSeparator)]
}

func MakeFormFile(e interface{}, fieldName, fileName string) (buf *bytes.Buffer, contentType *string, err error) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile(fieldName, fileName)
	if err != nil {
		return nil, nil, fmt.Errorf("bodyWriter.CreateFormFile. %v", err)
	}
	b, err := json.Marshal(e)
	if err != nil {
		return nil, nil, fmt.Errorf("json.Marshal. %v", err)
	}

	_, err = fileWriter.Write(b)
	if err != nil {
		return nil, nil, fmt.Errorf("fileWriter.Write %v", err)
	}
	bodyWriter.Close()
	c := bodyWriter.FormDataContentType()
	return bodyBuf, &c, nil
}

func PanicOnError(err error, msg string) {
	if err != nil {
		helperLog.Logger.Panic().Msgf("%s: %s", msg, err)
	}
}

func GetLocalIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, address := range addrs {
		// Check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}

	return "", fmt.Errorf("no IP address found")
}
