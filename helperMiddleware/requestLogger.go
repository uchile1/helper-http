package helperMiddleware

import (
	"bytes"
	"io"

	"github.com/gin-gonic/gin"
	"gitlab.com/uchile1/helper/helperLog"
)

// responseWriterWrapper es una estructura para envolver el ResponseWriter
type responseWriterWrapper struct {
	gin.ResponseWriter
	buf *bytes.Buffer
}

func (rw *responseWriterWrapper) Write(data []byte) (int, error) {
	// Escribe la respuesta en el ResponseWriter original
	n, err := rw.ResponseWriter.Write(data)

	// También escribe la respuesta en el buffer para capturarla
	rw.buf.Write(data)

	return n, err
}

func RequestLogger() gin.HandlerFunc {
	return func(c *gin.Context) {

		helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msg("---------Recibida una solicitud---------")
		// Leer el cuerpo de la solicitud
		buf := new(bytes.Buffer)
		_, err := buf.ReadFrom(c.Request.Body)
		if err != nil {
			helperLog.Logger.Error().Str("endpoint", c.Request.URL.RequestURI()).Msgf("Error al leer el cuerpo de la solicitud: %v", err)
		}

		// Restaurar el cuerpo de la solicitud para que pueda ser leído nuevamente más tarde
		c.Request.Body = io.NopCloser(buf)

		// Registrar el contenido del cuerpo de la solicitud
		helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msgf("Cuerpo de la solicitud:\n%s", buf)

		// Crear un nuevo ResponseWriter que capture la salida
		wrappedWriter := &responseWriterWrapper{ResponseWriter: c.Writer, buf: buf}
		c.Writer = wrappedWriter

		// Registrar los encabezados del request
		helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msg("Encabezados del request:")
		for key, values := range c.Request.Header {
			for _, value := range values {
				helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msgf("%s: %s", key, value)
			}
		}

		// Continuar con la solicitud
		c.Next()

		// Registra la respuesta
		helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msgf("Respuesta:\n%s", buf.String())

		helperLog.Logger.Debug().Str("endpoint", c.Request.URL.RequestURI()).Msg("---------Solicitud manejada---------")
	}
}
