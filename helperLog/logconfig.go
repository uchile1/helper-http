package helperLog

import (
	"os"
	"time"

	"github.com/rs/zerolog"
)

var Logger zerolog.Logger

func init() {
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano}
	multi := zerolog.MultiLevelWriter(consoleWriter)
	verbosidad := os.Getenv("LOG_LEVEL")
	level := zerolog.InfoLevel
	var err error
	if verbosidad != "" {
		level, err = zerolog.ParseLevel(verbosidad)
		if err != nil {
			os.Exit(1)
		}
	}

	Logger = zerolog.New(multi).With().Timestamp().Logger().Level(level)

}
